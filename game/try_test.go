package game

import (
	"../ban"
	"../game"
	"testing"
)

func TestTryTrue(t *testing.T) {
	//, 3   2   1
	//1|キ2|--0|ラ1|
	//2|--0|--0|--0|
	board := ban.Board{}
	board.Field[0][0] = ban.NewKoma(ban.Kirin, ban.Gote)
	board.Field[0][2] = ban.NewKoma(ban.Lion, ban.Sente)

	actual := game.Try(board, ban.Sente)
	expected := true

	if actual != expected {
		t.Errorf("got %v\nwant %v", actual, expected)
	}
}

func TestTryFalse(t *testing.T) {
	//, 3   2   1
	//1|--0|キ2|ラ1|
	//2|--0|--0|--0|
	board := ban.Board{}
	board.Field[0][1] = ban.NewKoma(ban.Kirin, ban.Gote)
	board.Field[0][2] = ban.NewKoma(ban.Lion, ban.Sente)

	actual := game.Try(board, ban.Sente)
	expected := false

	if actual != expected {
		t.Errorf("got %v\nwant %v", actual, expected)
	}
}

func TestTryFalse2(t *testing.T) {
	//, 3   2   1
	//1|--0|--0|ラ1|
	//2|--0|ぞ2|--0|
	board := ban.Board{}
	board.Field[1][1] = ban.NewKoma(ban.Zou, ban.Gote)
	board.Field[0][2] = ban.NewKoma(ban.Lion, ban.Sente)

	actual := game.Try(board, ban.Sente)
	expected := false

	if actual != expected {
		t.Errorf("got %v\nwant %v", actual, expected)
	}
}

func TestFindLion(t *testing.T) {
	board := ban.Board{}
	board.Field[0][2] = ban.NewKoma(ban.Lion, ban.Sente)

	actual := game.FindLion(board, ban.Sente)
	expected := [2]int{0, 2}

	if actual != expected {
		t.Errorf("got %v\nwant %v", actual, expected)
	}
}
