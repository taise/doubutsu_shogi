package game

import (
	"../ban"
	"../shared"
	"fmt"
)

// Lionが隣接する駒から取られないかチェックする
func Try(board ban.Board, sengo int) bool {
	lionPosition := FindLion(board, sengo)
	for i, row := range board.Field {
		for j, koma := range row {
			if koma.Sengo == sengo || koma.Sengo == ban.None {
				continue
			}
			from := [2]int{i, j}
			if shared.CanMoveIndex(koma, from, lionPosition) {
				return false
			}
		}
	}
	fmt.Println("--- Try ---")
	return true
}

func FindLion(board ban.Board, sengo int) [2]int {
	for i, row := range board.Field {
		for j, koma := range row {
			if koma.Animal.Type == ban.Lion && koma.Sengo == sengo {
				return [2]int{i, j}
			}
		}
	}
	return [2]int{-1, -1}
}
