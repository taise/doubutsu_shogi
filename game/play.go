package game

import (
	"../ban"
	"../player"
	"../shared"
)

func Start() {
	board := ban.NewBoard()
	teban := Teban{code: ban.Sente}
	tekazu := 1
	players := map[int]player.Who{
		//ban.Sente: player.User{ban.Sente},
		ban.Sente: player.AI{ban.Sente},
		ban.Gote:  player.AI{ban.Gote},
	}
	for {
		printGame(board, tekazu, teban)
		correctSashite := false
		from := [2]int{}
		to := [2]int{}
		if teban.code == ban.Gote {
			board = ban.TurnBoard(board)
		}
		for correctSashite == false {
			from, to = players[teban.code].Sashite(board, ban.Sente)
			correctSashite, _ = shared.IsCorrect(board, ban.Sente, from, to)
		}
		board, _ = sashite(board, ban.Sente, from, to)

		if teban.code == ban.Gote {
			board = ban.TurnBoard(board)
		}
		if checkGameEnd(board) {
			break
		}
		if tekazu > 256 {
			break
		}
		teban.change()
		tekazu++
	}
	printGameEnd(board, tekazu, teban)
}

func checkGameEnd(board ban.Board) bool {
	if tryCheck(board) {
		if Try(board, ban.Sente) {
			return true
		}
	}
	return board.TakeLion()
}

func tryCheck(board ban.Board) bool {
	for _, koma := range board.Field[0] {
		if koma.Animal.Type == ban.Lion && koma.Sengo == ban.Sente {
			return true
		}
	}
	return false
}
