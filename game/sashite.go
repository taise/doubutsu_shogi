package game

import (
	"../ban"
	"../shared"
)

func sashite(board ban.Board, sengo int, from [2]int, to [2]int) (ban.Board, string) {
	correct, errMsg := shared.IsCorrect(board, sengo, from, to)
	if correct == false {
		return board, errMsg
	}

	// 駒台から指す
	if from[0] == 0 {
		dai := board.Dais[sengo]
		animalType := from[1]
		dai.UseKoma(animalType)
		board.Update(to, ban.NewKoma(animalType, sengo))
		return board, ""
	}

	// 盤上から指す
	koma := board.GetKoma(from)
	torigoma := board.GetKoma(to)

	if koma.CanNari(to) {
		koma = ban.NewKoma(ban.Niwatori, sengo)
	}
	board.Update(from, ban.NewKoma(ban.Blank, ban.None))
	board.Update(to, koma)

	if torigoma.Sengo != ban.None {
		board.TakeKoma(sengo, torigoma)
	}
	return board, ""
}
