package game

import "../ban"

type Teban struct {
	code int
}

func (self *Teban) change() {
	if self.code == ban.Sente {
		self.code = ban.Gote
	} else {
		self.code = ban.Sente
	}
}

func (self Teban) name() string {
	if self.code == ban.Sente {
		return "先手"
	} else {
		return "後手"
	}
}
