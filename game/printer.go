package game

import (
	"../ban"
	"fmt"
)

func printGame(board ban.Board, tekazu int, teban Teban) {
	fmt.Println("--------------------------------------")
	fmt.Printf("# %v手目 ", tekazu)
	fmt.Println(teban.name())
	board.PrintField()
	fmt.Println()
	board.PrintDai()
}

func printGameEnd(board ban.Board, tekazu int, teban Teban) {
	fmt.Println("\n======================================\n")
	fmt.Printf("%v手をもって、%vの勝ちです\n\n", tekazu, teban.name())
	board.PrintField()
	fmt.Println()
	board.PrintDai()
}
