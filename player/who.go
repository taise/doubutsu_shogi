package player

import (
	"../ban"
)

type Who interface {
	Sashite(board ban.Board, sengo int) ([2]int, [2]int)
}
