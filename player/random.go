package player

import (
	"../ban"
	"math/rand"
	"time"
)

type AI struct {
	Sengo int
}

func randInt(max int) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(max)
}

func randPoint() [2]int {
	col := randInt(ban.Col) + 1
	row := randInt(ban.Row) + 1
	return [2]int{col, row}
}

func (ai AI) Sashite(board ban.Board, sengo int) ([2]int, [2]int) {
	dai := board.Dais[sengo]
	from := randPoint()
	if dai.HasKoma() {
		from = [2]int{0, randInt(4) + 1}
	}
	to := randPoint()
	return from, to
}
