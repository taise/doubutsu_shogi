package shared

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func ReadSashite() ([2]int, error) {
	from := [2]int{}
	reader := bufio.NewReader(os.Stdin)
	read1, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println(err)
		return from, err
	}
	inputFrom := strings.TrimSpace(read1)
	if len(inputFrom) > 2 {
		err := errors.New("reader: Too many input string.")
		fmt.Println(err)
		return from, err
	}

	for k, v := range inputFrom {
		if k >= 2 {
			break
		}
		from[k], err = strconv.Atoi(string(v))
		if err != nil {
			fmt.Println(err)
			return from, err
		}
	}
	fmt.Printf("%#v\n", from)
	return from, err
}
