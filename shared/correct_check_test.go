package shared

import (
	"../ban"
	"../shared"
	"testing"
)

func TestCorrectRange(t *testing.T) {
	actual := shared.IsCorrectRange([2]int{1, 1})
	if actual != true {
		t.Errorf("got %v\nwant %v", actual, true)
	}

	actual = shared.IsCorrectRange([2]int{ban.Col, ban.Row})
	if actual != true {
		t.Errorf("got %v\nwant %v", actual, false)
	}
}

func TestIsNotCorrectRange(t *testing.T) {
	actual := shared.IsCorrectRange([2]int{0, 0})
	if actual != false {
		t.Errorf("got %v\nwant %v", actual, false)
	}

	actual = shared.IsCorrectRange([2]int{ban.Row + 1, ban.Col + 1})
	if actual != false {
		t.Errorf("got %v\nwant %v", actual, false)
	}
}

func TestKifuToBanIndex(t *testing.T) {
	actual := ban.KifuToBanIndex([2]int{2, 3})
	expected := [2]int{2, 1}
	if actual != expected {
		t.Errorf("got %v\nwant %v", actual, expected)
	}
}

// 2322hi
//
// {0, 1}
//, 0   1   2
//0|   | 1 |   |
//1|   |ひ1|   |
//2|   |   |   |

func TestHiyokoCanMove(t *testing.T) {
	koma := ban.NewKoma(ban.Hiyoko, ban.Sente)
	from := [2]int{2, 3}
	to := [2]int{2, 2}
	actual := shared.CanMove(koma, from, to)
	if actual != true {
		t.Errorf("got %v\nwant %v", actual, true)
	}
}

func TestHiyokoCanNotMove(t *testing.T) {
	koma := ban.NewKoma(ban.Hiyoko, ban.Sente)
	from := [2]int{2, 3}
	to := [2]int{2, 4}
	actual := shared.CanMove(koma, from, to)
	if actual != false {
		t.Errorf("got %v\nwant %v", actual, false)
	}
}

// 2313ki
//, 3   2   1
//1|   |   |   |
//2|   |   |   |
//3|   |キ1|   |
//4|   |   |   |
func TestKirinCanMove(t *testing.T) {
	koma := ban.NewKoma(ban.Kirin, ban.Sente)
	from := [2]int{2, 3}
	to := [2]int{1, 3}
	actual := shared.CanMove(koma, from, to)
	if actual != true {
		t.Errorf("got %v\nwant %v", actual, true)
	}
}

func TestKirinCanNotMove(t *testing.T) {
	koma := ban.NewKoma(ban.Kirin, ban.Sente)
	from := [2]int{2, 3}
	to := [2]int{2, 1}
	actual := shared.CanMove(koma, from, to)
	if actual != false {
		t.Errorf("got %v\nwant %v", actual, false)
	}
}

func TestZouCanMove(t *testing.T) {
	koma := ban.NewKoma(ban.Zou, ban.Sente)
	from := [2]int{2, 3}
	to := [2]int{1, 4}
	actual := shared.CanMove(koma, from, to)
	if actual != true {
		t.Errorf("got %v\nwant %v", actual, true)
	}
}

func TestCanTakeFromDai(t *testing.T) {
	board := ban.NewBoard()
	board.Dais[ban.Sente][ban.Hiyoko] += 1
	from := [2]int{0, ban.Hiyoko}
	to := [2]int{1, 3}

	actual1, actual2 := shared.CanTakeFromDai(board, ban.Sente, from, to)
	if actual1 != true {
		t.Errorf("got %v\nwant %v", actual1, true)
	}
	if actual2 != "" {
		t.Errorf("got %v\nwant %v", actual2, "")
	}
}

func TestCanNotTakeFromDai(t *testing.T) {
	board := ban.NewBoard()
	from := [2]int{0, ban.Hiyoko}
	to := [2]int{1, 3}

	actual, _ := shared.CanTakeFromDai(board, ban.Sente, from, to)
	if actual != false {
		t.Errorf("got %v\nwant %v", actual, false)
	}
}

func TestCanNotPutFromDai(t *testing.T) {
	board := ban.NewBoard()
	from := [2]int{0, ban.Hiyoko}
	to := [2]int{2, 3}
	board.Dais[ban.Sente][ban.Hiyoko] += 1

	actual, _ := shared.CanTakeFromDai(board, ban.Sente, from, to)
	if actual != false {
		t.Errorf("got %v\nwant %v", actual, false)
	}
}
