package shared

import (
	"../ban"
)

func IsCorrect(board ban.Board, sengo int, from [2]int, to [2]int) (bool, string) {
	if from[0] == 0 {
		return CanTakeFromDai(board, sengo, from, to)
	}
	if IsCorrectRange(from) && IsCorrectRange(to) == false {
		return false, "盤外を指定しています"
	}
	koma := board.GetKoma(from)
	torigoma := board.GetKoma(to)
	if koma.Sengo == ban.None {
		return false, "そこに駒はありません"
	} else if koma.Sengo != sengo {
		return false, "相手の駒で打とうとしています"
	} else if torigoma.Sengo == sengo {
		return false, "自分の駒を取ろうとしています"
	}
	if CanMove(koma, from, to) == false {
		return false, "駒を移動できない場所です"
	}
	return true, ""
}

func CanTakeFromDai(board ban.Board, sengo int, from [2]int, to [2]int) (bool, string) {
	dai := board.Dais[sengo]
	animalType := from[1]
	if dai.CanUseKoma(animalType) != true {
		return false, "駒台に対象の駒がありません"
	}
	koma := board.GetKoma(to)
	if koma.Sengo != ban.None {
		return false, "駒がある場所には置けません"
	}
	return true, ""
}

func IsCorrectRange(point [2]int) bool {
	row, col := point[1], point[0]
	return (0 < row && row <= ban.Row &&
		0 < col && col <= ban.Col)
}

// 駒の盤上の位置から、移動可能かどうかをチェックする。
// Animal.Movableが1ならtrue
func CanMove(koma ban.Koma, from [2]int, to [2]int) bool {
	fromIndex := ban.KifuToBanIndex(from)
	toIndex := ban.KifuToBanIndex(to)
	return CanMoveIndex(koma, fromIndex, toIndex)
}

func CanMoveIndex(koma ban.Koma, from [2]int, to [2]int) bool {
	movableIndexes := ban.MovableBanIndexes(koma, from)
	for _, movableIndex := range movableIndexes {
		if movableIndex == to {
			return true
		}
	}
	return false
}
