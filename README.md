# Doubutsu Shogi

## structs

* Ban
* Koma

## Game

1. 先後を決める
2. ゲームの初期化
    * 駒を盤に置く
3. 指す
4. 終わり

### 指す

1. 自分の手番チェック
2. 指し手を選ぶ
    1. 打つことができる場所を探す
        * 持ち駒
          * 盤上
          * 駒台
    2. 打つことができる場所ごとのスコアを計算する
        * 自分の指し手が最もスコアが高くなるように
    3. 打つ
    4. 手番終わり


### AIを作る場合

* MinMax
-> 相手の手も評価しなければならない
-> 簡単に相手の手を評価するなら盤をひっくり返すのが早そう
* 配列のrow, colと指し手の列行が一致しない問題
-> 入力時にひっくり返す関数作れば良い
