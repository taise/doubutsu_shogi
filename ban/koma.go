package ban

import "strconv"

type Koma struct {
	Animal Animal
	Sengo  int
}

func NewKoma(animal int, sengo int) Koma {
	return Koma{NewAnimal(animal), sengo}
}

func (self *Koma) ToString() string {
	strSengo := strconv.Itoa(self.Sengo)
	return self.Animal.Name + strSengo
}

func (self *Koma) CanNari(to [2]int) bool {
	if self.Animal.Type == Hiyoko {
		if to[1] == 1 {
			return true
		}
	}
	return false
}
