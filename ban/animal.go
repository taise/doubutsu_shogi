package ban

type Animal struct {
	Name    string   // "ひ" (ひよこ)
	Type    int      // 2
	Movable [][2]int // {{0,1}}
}

const (
	Blank = iota
	Lion
	Hiyoko
	Zou
	Kirin
	Niwatori
)

func NewAnimal(animal int) Animal {
	switch {
	case animal == Lion:
		return Animal{
			Name: "ラ",
			Type: Lion,
			Movable: [][2]int{
				{0, 0}, {0, 1}, {0, 2},
				{1, 0}, {1, 2},
				{2, 0}, {2, 1}, {2, 2},
			},
		}
	case animal == Hiyoko:
		return Animal{
			Name: "ひ",
			Type: Hiyoko,
			Movable: [][2]int{
				{0, 1},
			},
		}
	case animal == Zou:
		return Animal{
			Name: "ぞ",
			Type: Zou,
			Movable: [][2]int{
				{0, 0}, {0, 2},
				{2, 0}, {2, 2},
			},
		}
	case animal == Kirin:
		return Animal{
			Name: "キ",
			Type: Kirin,
			Movable: [][2]int{
				{0, 1},
				{1, 0}, {1, 2},
				{2, 1},
			},
		}
	case animal == Niwatori:
		return Animal{
			Name: "に",
			Type: Niwatori,
			Movable: [][2]int{
				{0, 0}, {0, 1}, {0, 2},
				{1, 0}, {1, 2},
				{2, 1},
			},
		}
	}
	return Animal{
		Name:    "--",
		Type:    Blank,
		Movable: [][2]int{},
	}
}

func GetAnimalName(animal int) string {
	name := ""
	switch {
	case animal == Lion:
		name = "ラ"
	case animal == Hiyoko:
		name = "ひ"
	case animal == Zou:
		name = "ぞ"
	case animal == Kirin:
		name = "キ"
	case animal == Niwatori:
		name = "に"
	}
	return name
}

// 21hi
// Movable {{0, 1}} -> Movable {{1, 1}}
//, 0   1   2
//0|   |   |   |
//1|   | 1 |   |
//2|   |ひ1|   |
//3|   |   |   |
func MovableBanIndexes(koma Koma, position [2]int) [][2]int {
	movables := koma.Animal.Movable
	absPosition := [2]int{position[0] - 1, position[1] - 1}
	resultIndexes := [][2]int{}
	for _, movable := range movables {
		row := movable[0] + absPosition[0]
		col := movable[1] + absPosition[1]
		if 0 <= row && row < Row && 0 <= col && col < Col {
			resultIndexes = append(resultIndexes, [2]int{row, col})
		}
	}
	return resultIndexes
}
