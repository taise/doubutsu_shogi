package ban

import (
	"../ban"
	"testing"
)

func IsEqualIndexes(s1 [][2]int, s2 [][2]int) bool {
	for i, _ := range s1 {
		if s1[i] != s2[i] {
			return false
		}
	}
	return true
}

func TestGetAnimalName(t *testing.T) {
	actual := ban.GetAnimalName(ban.Hiyoko)
	expected := "ひ"
	if actual != expected {
		t.Errorf("got %v\nwant %v", actual, expected)
	}
}

func TestMovableBanIndexesHiyoko(t *testing.T) {
	koma := ban.NewKoma(ban.Hiyoko, ban.Sente)
	position := [2]int{2, 1}
	actual := ban.MovableBanIndexes(koma, position)
	expected := [][2]int{{1, 1}}

	if IsEqualIndexes(actual, expected) != true {
		t.Errorf("got %v\nwant %v", actual, expected)
	}
}

func TestMovableBanIndexesKirin(t *testing.T) {
	koma := ban.NewKoma(ban.Kirin, ban.Sente)
	position := [2]int{3, 2}
	actual := ban.MovableBanIndexes(koma, position)
	expected := [][2]int{{2, 2}, {3, 1}}

	if len(actual) != len(expected) {
		t.Errorf("got %v\nwant %v", len(actual), len(expected))
	}
	if IsEqualIndexes(actual, expected) != true {
		t.Errorf("got %v\nwant %v", actual, expected)
	}
}

func TestMovableBanIndexesZou(t *testing.T) {
	koma := ban.NewKoma(ban.Zou, ban.Sente)
	position := [2]int{2, 1}
	actual := ban.MovableBanIndexes(koma, position)
	expected := [][2]int{{1, 0}, {1, 2}, {3, 0}, {3, 2}}

	if len(actual) != len(expected) {
		t.Errorf("got %v\nwant %v", len(actual), len(expected))
	}
	if IsEqualIndexes(actual, expected) != true {
		t.Errorf("got %v\nwant %v", actual, expected)
	}
}
