package ban

import (
	"fmt"
)

const (
	Row = 4
	Col = 3
)

const (
	None  = 0
	Sente = 1
	Gote  = 2
)

type Board struct {
	Field [Row][Col]Koma
	Dais  map[int]Dai
}

func NewBoard() Board {
	// [
	//   [ぞ2, ラ2, キ2],
	//   [--0, ひ2, --0],
	//   [--0, ひ1, --0],
	//   [キ1, ラ1, ぞ1],
	// ]
	return Board{
		Field: [Row][Col]Koma{
			{NewKoma(Kirin, Gote), NewKoma(Lion, Gote), NewKoma(Zou, Gote)},
			{NewKoma(Blank, None), NewKoma(Hiyoko, Gote), NewKoma(Blank, None)},
			{NewKoma(Blank, None), NewKoma(Hiyoko, Sente), NewKoma(Blank, None)},
			{NewKoma(Zou, Sente), NewKoma(Lion, Sente), NewKoma(Kirin, Sente)},
		},
		Dais: map[int]Dai{Sente: NewDai(), Gote: NewDai()},
	}
}

func (self Board) PrintDai() {
	fmt.Printf("先手 [%v]\n", self.Dais[Sente].ToString())
	fmt.Printf("後手 [%v]\n", self.Dais[Gote].ToString())
}

func (self Board) PrintField() {
	// Print format: 駒名 + 先手/後手
	fmt.Println(", 3   2   1")
	for i, rows := range self.Field {
		fmt.Printf("%v|", i+1)
		for _, koma := range rows {
			fmt.Printf("%v|", koma.ToString())
		}
		fmt.Printf("\n")
	}
}

func (self Board) GetKoma(point [2]int) Koma {
	index := KifuToBanIndex(point)
	return self.Field[index[0]][index[1]]
}

func (self *Board) Update(point [2]int, koma Koma) {
	index := KifuToBanIndex(point)
	self.Field[index[0]][index[1]] = koma
}

func (self Board) TakeLion() bool {
	if self.Dais[Sente][Lion] > 0 || self.Dais[Gote][Lion] > 0 {
		return true
	} else {
		return false
	}
}

func (self *Board) TakeKoma(sengo int, koma Koma) {
	if koma.Animal.Type == Niwatori {
		koma = NewKoma(Hiyoko, sengo)
	}
	dai := self.Dais[sengo]
	self.Dais[sengo] = AddKoma(dai, koma)
}

// 23hi -> 21
//, 3   2   1
//1|   |   |   |
//2|   |   |   |
//3|   |ひ1|   |
//4|   |   |   |

//, 0   1   2
//0|   |   |   |
//1|   |   |   |
//2|   |ひ1|   |
//3|   |   |   |
func KifuToBanIndex(point [2]int) [2]int {
	return [2]int{point[1] - 1, 2 - (point[0] - 1)}
}

func TurnBoard(board Board) Board {
	newBoard := Board{}

	// 逆順に入れ替え
	for i := range board.Field {
		k := Row - i
		for j := range board.Field[i] {
			l := Col - j
			point := [2]int{l, k}
			koma := board.Field[i][j]
			if koma.Sengo == Sente {
				koma.Sengo = Gote
			} else if koma.Sengo == Gote {
				koma.Sengo = Sente
			}
			newBoard.Update(point, koma)
		}
	}

	// change dais sengo
	newBoard.Dais = map[int]Dai{
		Sente: board.Dais[Gote],
		Gote:  board.Dais[Sente],
	}
	return newBoard
}
