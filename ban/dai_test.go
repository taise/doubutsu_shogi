package ban

import (
	"../ban"
	"testing"
)

func TestAddKoma(t *testing.T) {
	dai := ban.NewDai()
	koma := ban.NewKoma(ban.Hiyoko, ban.Sente)
	actual := ban.AddKoma(dai, koma)
	expected := ban.Dai{
		ban.Lion:   0,
		ban.Hiyoko: 1,
		ban.Kirin:  0,
		ban.Zou:    0,
	}

	if actual[ban.Hiyoko] != expected[ban.Hiyoko] {
		t.Errorf("got %v\nwant %v", actual, expected)
	}

}

func TestHasNotKoma(t *testing.T) {
	dai := ban.NewDai()
	actual := dai.HasKoma()
	expected := false

	if actual != expected {
		t.Errorf("got %v\nwant %v", actual, expected)
	}
}

func TestCanUseKoma(t *testing.T) {
	actual := ban.Dai{ban.Hiyoko: 1}
	result := actual.UseKoma(ban.Hiyoko)

	if result != true {
		t.Errorf("got %v\nwant %v", result, true)
	}

	if actual[ban.Hiyoko] != 0 {
		t.Errorf("got %v\nwant %v", actual, 0)
	}
}

func TestUseExistsKoma(t *testing.T) {
	actual := ban.Dai{ban.Hiyoko: 1}
	result := actual.UseKoma(ban.Hiyoko)

	if result != true {
		t.Errorf("got %v\nwant %v", result, true)
	}

	if actual[ban.Hiyoko] != 0 {
		t.Errorf("got %v\nwant %v", actual, 0)
	}
}
