package ban

import (
	"fmt"
	"sort"
)

type Dai map[int]int

func NewDai() Dai {
	return Dai{
		Lion:   0,
		Hiyoko: 0,
		Kirin:  0,
		Zou:    0,
	}
}

func (self Dai) GetOrderedKey() []int {
	keys := []int{}
	for k := range self {
		keys = append(keys, k)
	}
	sort.Ints(keys)
	return keys
}

func (self Dai) ToString() string {
	var str string = ""
	for _, j := range self.GetOrderedKey() {
		animalName := GetAnimalName(j)
		str += fmt.Sprintf("%v: %v, ", animalName, self[j])
	}
	return str
}

func AddKoma(dai Dai, torikoma Koma) Dai {
	dai[torikoma.Animal.Type] += 1
	return dai
}

func (self Dai) HasKoma() bool {
	for _, v := range self {
		if v > 0 {
			return true
		}
	}
	return false
}

func (self Dai) CanUseKoma(animalType int) bool {
	if self[animalType] > 0 {
		return true
	} else {
		return false
	}
}

func (self Dai) UseKoma(animalType int) bool {
	if self[animalType] > 0 {
		self[animalType] -= 1
		return true
	} else {
		return false
	}
}
